import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Dish} from "./model/dish";
import {Order} from "./model/order";

@Injectable()
export class ApiService {
  private apiurl = 'http://localhost:3000/';

  constructor(private http: HttpClient) {
  }

  public getDishes(): Observable<any> {
    return this.http.get<any>(this.apiurl + 'dishes');
  }

  public sendOrder(requestBody: Order): Observable<any> {
    console.log(requestBody);
    const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    let body = new HttpParams();
    body = body.set('address', requestBody.address);
    body = body.set('dishes', JSON.stringify(requestBody.dishes));
    body = body.set('date', requestBody.date.toString());
    body = body.set('time', requestBody.time);
    body = body.set('amount', requestBody.amount.toString());
    return this.http.post<Order>(this.apiurl + 'orders', body, {headers: myheader});
  }
}
