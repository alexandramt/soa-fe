interface Dish {
  name: string;
  quantity: number;
}
export interface IOrder {
  address: string;
  time: string;
  amount: number;
  date: Date;
  dishes: Dish[];
}

export class Order implements IOrder{
  address: string;
  time: string;
  amount: number;
  date: Date;
  dishes: Dish[];

  constructor() {
    this.address = '';
    this.time = '';
    this.amount = 0;
    this.date = new Date();
    this.dishes = [];
  }
}

