import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Dish} from "./model/dish";
import {ApiService} from "./api.service";
import {Order} from "./model/order";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  totalAmount: number;
  success = false;

  dishes: Array<Dish> = [];

  constructor(private _formBuilder: FormBuilder,
              private apiService: ApiService) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      // dishesCtrl: [this.dishes, Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      addressCtrl: ['', Validators.required],
      dateCtrl: ['', Validators.required],
      timeCtrl: ['10:00', Validators.required]
    });

    this.getDishes();
  }

  public onQuantityChanged(dish, quantity) {
    this.dishes.forEach(item => {
      if(item === dish) {
        item.quantity = quantity;
      }
    });
    console.log(this.dishes);
    this.totalAmount = this.computeAmount();
  }
  private computeAmount() {
    let total = 0;
    this.dishes.forEach(item => {
        if(item.quantity > 0) {
          total += item.price * item.quantity;
        }
      });
    return total;
  }
  public getDishes() {
    this.apiService.getDishes().subscribe(
      (result) => {
        this.dishes = result.data.map(item => {
          return new Dish(item);
        });
        console.log(this.dishes);
      },
      (error) => {
        console.log(error);
      }
    )
  }


  public sendOrder() {
    let order = this.createOrderRequestBody();
    this.apiService.sendOrder(order).subscribe(
      result => {
        this.success = true;
        console.log(result);
      },
      error => {
        console.log(error);
      }
    );
    console.log(order);
  }

  private createOrderRequestBody(): Order {
    let order = new Order();
    this.dishes.forEach(item => {
      if(item.quantity > 0) {
        order.dishes.push({
          name: item.name,
          quantity: item.quantity
        })
      }
    });
    order.amount = this.totalAmount;
    order.address = this.secondFormGroup.controls.addressCtrl.value;
    order.date = this.secondFormGroup.controls.dateCtrl.value;
    order.time = this.secondFormGroup.controls.timeCtrl.value;
    return order;
  }
}
